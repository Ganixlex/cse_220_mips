#Homework #1
#Name: Xiang Li
#Net ID: xiangli5
#SBU ID: 110445032

.data
# include the file with the test case information
.include "Header3.asm" #change this line to test with other inputs
.align 2
numargs: .word 0
AddressOfIPDest3: .word 0
AddressOfIPDest2: .word 0
AddressOfIPDest1: .word 0
AddressOfIPDest0: .word 0
AddressOfBytesSent: .word 0
AddressOfPayload: .word 0
Err_string: .asciiz "ERROR\n"
newline: .asciiz "\n"
v4: .asciiz "IPv4\n"
comma:.asciiz ","
period: .asciiz "."
other_version: .asciiz "Unsupported: IPv"


# Helper macro for accessing command line arguments via Label
.macro print_comma
    la $a0,comma
    li $v0,4
    syscall
.end_macro

.macro print_newline
    la $a0,newline
    li $v0,4
    syscall
.end_macro

.macro print_period
    la $a0,period
    li $v0,4
    syscall
.end_macro 

.macro load_args
    sw $a0, numargs
    lw $t0, 0($a1)
    sw $t0, AddressOfIPDest3
    lw $t0, 4($a1)
    sw $t0, AddressOfIPDest2
    lw $t0, 8($a1)
    sw $t0, AddressOfIPDest1
    lw $t0, 12($a1)
    sw $t0, AddressOfIPDest0
    lw $t0, 16($a1)
    sw $t0, AddressOfBytesSent
    lw $t0, 20($a1)
    sw $t0, AddressOfPayload
.end_macro


.text 
.globl main
main:
    load_args()
    check_input_arg:
        lw $t0,numargs
        bne $t0,6,Err_Exit
    check_IPDest:
    	lw $a0,AddressOfIPDest0
    	li $v0,84
    	syscall
    	bne $v1,0,Err_Exit
    	jal check_range
    	lw $a0,AddressOfIPDest1
    	li $v0,84
    	syscall
    	bne $v1,0,Err_Exit
    	jal check_range
    	lw $a0,AddressOfIPDest2
    	li $v0,84
    	syscall
    	bne $v1,0,Err_Exit
    	jal check_range
    	lw $a0,AddressOfIPDest3
    	li $v0,84
    	syscall
    	bne $v1,0,Err_Exit
    	jal check_range
    check_BytesSent:
    	lw $a0,AddressOfBytesSent
    	li $v0,84
    	syscall
    	bne $v1,0,Err_Exit
    	blt $v0,-1,Err_Exit
    	beq $v0,-1,check_Pversion
    	bgt $v0,8191,Err_Exit
    	li $t1,8
    	div $v0,$t1
    	mfhi $t1
    	
    	bne $t1,0,Err_Exit
    	
    
    check_Pversion:
    	la $t0,Header #the starting of the header
    	lb $t0,3($t0)
    	
    	sll $t0,$t0,24 #get rid of f's if negative
    	srl $t0,$t0,24
    	sra $t1,$t0,4 #t1 now contains the version number
    	
    	bne $t1,4,unsupport
    	
    	la $a0,v4 #print the IPv4 version
    	li $v0,4
    	syscall
    	j print_SITP
    	
    	unsupport:
    	    la $a0,other_version #print the other version type
    	    li $v0,4
    	    syscall
    	    move $a0,$t1
    	    li $v0,1
    	    syscall
    	    la $a0,newline
    	    li $v0,4
    	    syscall
    	    change_version: #change the version number to 4
    	    	la $t0,Header
    	    	lb $t1,3($t0)
    	    	li $t2,0xffffff0f #clear that bit
    	    	li $t3,0x00000040 #setting that bit to 4
    	    	and $t4,$t1,$t2
    	    	or $t4, $t3,$t4
    	    	sb $t4,3($t0)
    	    		
    print_SITP: #print service, identifier, time, and protocals
    	    
    	la $t0,Header #Service
    	lb $t1,2($t0)
        li $t2,0x000000ff
        and $t1,$t1,$t2
        move $a0,$t1
    	li $v0,1
    	syscall
    	    
    	print_comma()
    	    
    	la $t0,Header #Identifier
    	lb $t1,7($t0)
    	lb $t2,6($t0)
    	sll $t1,$t1,8
  	li $t3,0xffff00ff
  	and $t2,$t2,$t3
    	or $t1,$t1,$t2
    	li $t4,0x0000ffff
    	and $t1,$t1,$t4
    	move $a0,$t1
    	li $v0,1
    	syscall
    	    
    	print_comma()
    	    
    	la $t0,Header #Time to live
    	lb $t1,11($t0)
    	li $t2,0x000000ff
    	and $t1,$t2,$t1
    	move $a0,$t1
    	li $v0,1
    	syscall
    	    
    	print_comma()
    	    
    	la $t0,Header #Protocal
    	lb $t1,10($t0)
    	li $t2,0x000000ff
    	and $t1,$t2,$t1
    	move $a0,$t1
    	li $v0,1
    	syscall
    	    
    	print_newline()
    	
    print_IPSrcs:
    	li $t2,0x000000ff
    	
    	la $t0,Header
    	lb $t1,15($t0)
    	and $t1,$t1,$t2
    	move $a0,$t1
    	li $v0,1
    	syscall
    	
    	print_period()
    	
    	lb $t1,14($t0)
    	and $t1,$t1,$t2
    	move $a0,$t1
    	li $v0,1
    	syscall
    	
    	print_period()
    	
    	lb $t1,13($t0)
    	and $t1,$t1,$t2
    	move $a0,$t1
    	li $v0,1
    	syscall
    	
    	print_period()
    	
    	lb $t1,12($t0)
    	and $t1,$t1,$t2
    	move $a0,$t1
    	li $v0,1
    	syscall
    	
    	print_newline()
    	
    set_IPDest:
    	lw $a0,AddressOfIPDest0
    	li $v0,84
    	syscall
    	move $s0,$v0 #s0 now have the number of Dest0
    	lw $a0,AddressOfIPDest1
    	li $v0,84
    	syscall
    	move $s1,$v0 #s1 have Dest1
    	lw $a0,AddressOfIPDest2
    	li $v0,84
    	syscall
    	move $s2,$v0 #s2 have Dest2
    	lw $a0,AddressOfIPDest3
    	li $v0,84
    	syscall
    	move $s3,$v0 #s3 have Dest3
    	
    	la $t0,Header
    	sb $s3,19($t0)
    	sb $s2,18($t0)
    	sb $s1,17($t0)
    	sb $s0,16($t0)
    	
    	print_IPDest:
    	    lw $a0,16($t0)
    	    li $v0,34
    	    syscall
    	    print_newline()
    	    
    count_Payload:
    	lw $t0, AddressOfPayload
    	li $t2,0 #a counter
    	payload_loop:
    	    lb $t1,($t0)
    	    beqz $t1,exit_loop
    	    addi $t2,$t2,1
    	    addi $t0,$t0,1
    	    j payload_loop
    	exit_loop:
    	    move $s0,$t2 #s0 now saves the number of bytes for payload
    	
    	get_Header_Length:
    	    la $t0,Header
    	    lb $t1,3($t0)
    	    li $t2,0x0000000f
    	    and $t1,$t2,$t1
    	add_Length:
    	    li $t9,4
    	    mul $t1,$t1,$t9
    	    add $s0,$s0,$t1
    	save_to_TotalLength:
    	    la $t0,Header
    	    sh $s0,($t0)
    	
    	
    flags_fragments:
    	print_flag:
    	    la $t0,Header
    	    lb $t1,5($t0)
    	    srl $t1,$t1,5
    	    sll $t1,$t1,29
    	    srl $t1,$t1,29
    	    move $a0,$t1
    	    li $v0,35
    	    syscall
    	    
    	print_comma()
    	
    	print_fragments:
    	    la $t0,Header
    	    lb $t1,5($t0)
    	    sll $t1,$t1,27
    	    sra $t1,$t1,19
    	    
    	    lb $t2,4($t0)
    	    li $t3,0x000000ff
    	    and $t2,$t2,$t3
    	    or $t1,$t1,$t2
    	    
    	    li $t4,0x00001fff
    	    and $t1,$t1,$t4
    	    
    	    move $a0,$t1
    	    li $v0,35
    	    syscall
    	    print_newline()
    	count_BytesSent:
    	    lw $a0,AddressOfBytesSent
    	    li $v0,84
    	    syscall
    	    
    	    move $s0,$v0 #save the number of bytes send to s0
    	    beqz $s0,bytes_send_zero
    	    beq $s0,-1,bytes_send_negative
    	    bgtz $s0,bytes_send_positive
    	    
    	    bytes_send_zero:
    	    
    	    	la $t0,Header
    	    	li $t1,0x0000
    	    	sh $t1,4($t0)
    	        
    	        j finish_count_bytesSent
    	        
    	    bytes_send_negative:
    	    
    	        la $t0,Header
    	        li $t1,0x4000
    	        sh $t1,4($t0)
    	    
    	        j finish_count_bytesSent
    	    
    	    bytes_send_positive:
    	   	#set flag
    	        la $t0,Header
    	        lb $t1,5($t0)
    	        li $t2,0x9f
    	        and $t1,$t1,$t2
    	        sb $t1,5($t0)
    	        
    	       	#set fragments
    	       	la $t0,Header
    	       	move $t1,$s0
    	       	srl $t1,$t1,8
    	       	li $t2,0x80
    	       	or $t1,$t2,$t1
    	       	sb $t1,5($t0)
    	       	
    	       	move $t1,$s0
    	       	sll $t1,$t1,24
    	       	srl $t1,$t1,24
    	       	sb $t1,4($t0)
    	       	
    	       
    	        j finish_count_bytesSent
    	   
    	    
    	finish_count_bytesSent:
    	    
    	store_payload:
    	    get_header_length:
    	        la $t0,Header
    	        lb $t1,3($t0)
    	    	sll $t1,$t1,28
    	    	srl $t1,$t1,28 #t1 now has the header length
    	    	
    	    calc_store_address:
    	    	li $t2,4
    	    	mul $t1,$t1,$t2
    	    	add $t0,$t0,$t1 #t0 now is the starting address of payload storage
    	    	lw $s1,AddressOfPayload
    	    store_payload_loop:
    	    	lb $t1,($s1)
    	    	beqz $t1,exit_payload_loop
    	    	sb $t1,($t0)
    	    	addi $s1,$s1,1
    	    	addi $t0,$t0,1
    	    	j store_payload_loop
    	    exit_payload_loop:
    	    	
	print_start_end:
	    #this prints the header
	    la $t0,Header
	    move $a0,$t0
	    li $v0,34
	    syscall
	    
	    print_comma()
    
    	    la $t0,Header
    	    lh $t1,($t0)
    	    or $a0,$t0,$t1
    	    li $v0,34
    	    syscall
    	    
    	    print_newline()
    	    
    	checksum:
    	    la $t0,Header
    	    set_intial_checksum_value:
    	    	li $t2,0
    	    	sh $t2,8($t0)
    	    	
    	    li $t2,0
    	    li $t3,0
    	    calc_checksum:
    	    	lh $t1,($t0)
    	    	li $t4,0x0000ffff
    	    	and $t1,$t1,$t4
    	    	addi $t0,$t0,2
    	    	addi $t2,$t2,1
    	    	bgt $t2,10,exit_calc_checksum
    	    	add $t3,$t3,$t1
    	    	j calc_checksum
    	    exit_calc_checksum:
    	    	srl $t0,$t3,16
    	    	sll $t1,$t3,16
    	    	srl $t1,$t1,16
    	    	add $t0,$t0,$t1
    	    	li $t1,0xffff0000
    	    	nor $t0,$t0,$t1 #$t0 now has the fliped bits
    	    	move $t8,$t0 
    	    save_checksum:
    	    	la $t0, Header
    	    	sh $t8,8($t0)
	
    	
    j Exit
check_range:
    blt $v0,0,Err_Exit
    bgt $v0,255,Err_Exit
    jr $ra
Err_Exit:
    la $a0,Err_string
    li $v0,4
    syscall
    
    li $v0,10
    syscall
Exit:
    li $v0,10
    syscall 

