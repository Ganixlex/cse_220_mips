##############################################################
# Homework #4
# name: Xiang Li
# sbuid: 110445032
##############################################################

.text
####HELPER FUNCTION###########
get_cell:
    #a0-> board, a1->n_col, a2->row, a3->col 
    #return v0 as the address of that cell
    li $t0,2
    mul $t1,$a1,$t0
    mul $t1,$t1,$a2
    mul $t0,$t0,$a3
    add $t0,$t0,$t1
    add $v0,$a0,$t0
    jr $ra
    
###############################
#Part I
clear_board:
    #a0 -> starting address of the board
    #a1 -> row number a2-> col number
    
    blt $a1,2,clear_board_error
    blt $a2,2,clear_board_error
    
    li $t0,-1 #the row number counter
    row_clear:
    	li $t1,0 #the col number counter
    	addi $t0,$t0,1
    	beq $t0,$a1,finish_clear	
    	col_clear:
    	    li $t2,2
    	    mul $t3,$t2,$t1 #size_obj *j
    	    mul $t4,$a2,$t2 #row_size
    	    mul $t4,$t4,$t0 #row_size*i
    	    add $t4,$t4,$t3
    	    add $t5,$a0,$t4 #t5 now contains the address of the object
    	    li $t2,-1
    	    sh $t2,($t5)
    	    addi $t1,$t1,1
    	    beq $t1,$a2,row_clear
    	    j col_clear
    finish_clear:
    	li $v0,0
        jr $ra
    clear_board_error:
        li $v0,-1
        jr $ra	  

#######################################################
place:
    #a0 -> address of board, a1 -> n_rows, a2 -> n_cols, a3 ->row, 0($sp) -> col, 4($sp) -> val
    lw $t0,($sp)
    lw $t1,4($sp)
    
    blt $a1,2,place_error
    blt $a2,2,place_error
    
    bltz $a3,place_error
    bge $a3,$a1,place_error
    
    bltz $t0,place_error
    bge $t0,$a2,place_error
    
    bge $t1,2,check_power2
    bne $t1,-1,place_error
    j place_it
    check_power2:
    	addi $t2,$t1,-1
    	and $t2,$t1,$t2
    	bnez $t2,place_error
    place_it:
    	li $t2,2
    	mul $t3,$t2,$t0 #size_obj *j
    	mul $t4,$a2,$t2 #row_size
    	mul $t4,$t4,$a3 #row_size*i
    	add $t4,$t4,$t3
    	add $t5,$a0,$t4 #t5 now contains the address of the object
    	sh $t1,($t5)
    place_success:
    	li $v0,0
    	jr $ra
    place_error:
    	li $v0,-1
    	jr $ra
        	    
################################################
start_game:
    #a0 -> board, a1 -> n_rows, a2-> n_cols, a3 ->r1,($sp) -> c1, 4($sp) -> r2, 8($sp)->c2
    addi $sp,$sp,-4
    sw $ra, ($sp)
    jal clear_board
    lw $ra,($sp)
    addi $sp,$sp,4
    bnez $v0,start_game_error
    
    lw $t0,($sp) #c1
    lw $t1,4($sp) #r2
    lw $t2,8($sp)  #c2
    
    bltz $a3,start_game_error
    bge $a3,$a1,start_game_error
    bltz $t1,start_game_error
    bge $t1,$a1,start_game_error
    bltz $t0,start_game_error
    bge $t0,$a2,start_game_error
    bltz $t2,start_game_error
    bge $t2,$a2,start_game_error
   
    addi $sp,$sp,-20
    sw $t0,($sp)
    li $t0,2
    sw $t0,4($sp)
    sw $ra,8($sp)
    sw $t1,12($sp)
    sw $t2,16($sp)
    
    jal place
    
    lw $t0,($sp)
    lw $ra,8($sp)
    lw $t1,12($sp)
    lw $t2,16($sp)
    addi $sp,$sp,20
   
    move $a3,$t1
    addi $sp,$sp,-12
    sw $t2,($sp)
    li $t0,2
    sw $t0,4($sp)
    sw $ra,8($sp)
    
    jal place
    
    lw $ra,8($sp)
    addi $sp,$sp,12
    
    
    start_game_success:
    	li $v0,0
    	jr $ra
    
    
    start_game_error:
    	li $v0,-1
    	jr $ra
           	            	        	    
#Part II
#################################################
merge_row:
    #a0 -> board, a1 -> n_rows, a2 -> n_cols, a3 -> row, ($sp)-> direction
    lw $t0,($sp)
    move $t5,$a2
    bltz $a3,merge_row_error
    bge $a3,$a1,merge_row_error
    blt $a1,2,merge_row_error
    blt $a2,2,merge_row_error
    
    beqz $t0,row_LtoR
    beq $t0,1,row_RtoL
    j merge_row_error
    
    row_LtoR:
    	li $t0,0 #counter 1 ->col 1
    	li $t1,1 #counter 2 ->col 2
    	for_LtoR:
    	    addi $sp,$sp,-24
    	    sw $a1,($sp)
    	    sw $a2,4($sp)
    	    sw $a3,8($sp)
    	    sw $t0,12($sp)
    	    sw $t1,16($sp)
    	    sw $ra,20($sp)
    	    
    	    lw $a1,4($sp)
    	    lw $a2,8($sp)
    	    move $a3,$t0
    	    
    	    jal get_cell
    	    
    	    lw $a1,($sp)
    	    lw $a2,4($sp)
    	    lw $a3,8($sp)
    	    lw $t0,12($sp)
    	    lw $t1,16($sp)
    	    lw $ra,20($sp)
    	    addi $sp,$sp,24
    	    move $t8,$v0 #now the address of the first cell is store in $t8
    	    #########
    	    addi $sp,$sp,-24
    	    sw $a1,($sp)
    	    sw $a2,4($sp)
    	    sw $a3,8($sp)
    	    sw $t0,12($sp)
    	    sw $t1,16($sp)
    	    sw $ra,20($sp)
    	    
    	    lw $a1,4($sp)
    	    lw $a2,8($sp)
    	    move $a3,$t1
    	    
    	    jal get_cell
    	    
    	    lw $a1,($sp)
    	    lw $a2,4($sp)
    	    lw $a3,8($sp)
    	    lw $t0,12($sp)
    	    lw $t1,16($sp)
    	    lw $ra,20($sp)
    	    addi $sp,$sp,24
    	    move $t9,$v0 #the address of the second one is store at t9
    	    
    	    lh $t6,($t8)
    	    lh $t7,($t9) #t6,t7 now contains the number of each cell
    	    
    	    add $t4,$t6,$t7
    	    beq $t4,-2,cont_merge_LtoR
    	    beq $t6,$t7,merge_LtoR
    	    
    	    cont_merge_LtoR:
    	    addi $t0,$t0,1
    	    addi $t1,$t1,1
    	    beq $t1,$a2,exit_for_LtoR
    	    j for_LtoR
    	    merge_LtoR:
    	    	li $t2,2
    	    	mul $t6,$t6,$t2
    	    	sh $t6,($t8)
    	    	li $t2,-1
    	    	sh $t2,($t9)
    	    	addi $t5,$t5,-1 #this is a counter of how many cells left
    	    	addi $t0,$t0,1
    	    	addi $t1,$t1,1
    	    	beq $t1,$a2,exit_for_LtoR
    	    	j for_LtoR
    	    
    	    exit_for_LtoR:
    	    	j merge_row_success
    row_RtoL:
    	addi $t0,$a2,-1 #counter 1 ->col 1
    	addi $t1,$a2,-2 #counter 2 ->col 2
    	for_RtoL:
    	    addi $sp,$sp,-24
    	    sw $a1,($sp)
    	    sw $a2,4($sp)
    	    sw $a3,8($sp)
    	    sw $t0,12($sp)
    	    sw $t1,16($sp)
    	    sw $ra,20($sp)
    	    
    	    lw $a1,4($sp)
    	    lw $a2,8($sp)
    	    move $a3,$t0
    	    
    	    jal get_cell
    	    
    	    lw $a1,($sp)
    	    lw $a2,4($sp)
    	    lw $a3,8($sp)
    	    lw $t0,12($sp)
    	    lw $t1,16($sp)
    	    lw $ra,20($sp)
    	    addi $sp,$sp,24
    	    move $t8,$v0 #now the address of the first cell is store in $t8
    	    #########
    	    addi $sp,$sp,-24
    	    sw $a1,($sp)
    	    sw $a2,4($sp)
    	    sw $a3,8($sp)
    	    sw $t0,12($sp)
    	    sw $t1,16($sp)
    	    sw $ra,20($sp)
    	    
    	    lw $a1,4($sp)
    	    lw $a2,8($sp)
    	    move $a3,$t1
    	    
    	    jal get_cell
    	    
    	    lw $a1,($sp)
    	    lw $a2,4($sp)
    	    lw $a3,8($sp)
    	    lw $t0,12($sp)
    	    lw $t1,16($sp)
    	    lw $ra,20($sp)
    	    addi $sp,$sp,24
    	    move $t9,$v0 #the address of the second one is store at t9
    	    
    	    lh $t6,($t8)
    	    lh $t7,($t9) #t6,t7 now contains the number of each cell
    	    
    	    add $t4,$t6,$t7
    	    beq $t4,-2,cont_merge_RtoL
    	    
    	    beq $t6,$t7,merge_RtoL
    	    
    	    cont_merge_RtoL:
    	    addi $t0,$t0,-1
    	    addi $t1,$t1,-1
    	    bltz $t1,exit_for_RtoL
    	    j for_RtoL
    	    merge_RtoL:
    	    	li $t2,2
    	    	mul $t6,$t6,$t2
    	    	sh $t6,($t8)
    	    	li $t2,-1
    	    	sh $t2,($t9)
    	    	addi $t5,$t5,-1 #this is a counter of how many cells left
    	     	addi $t0,$t0,-1
    	    	addi $t1,$t1,-1
    	    	bltz $t1,exit_for_RtoL
    	    	j for_RtoL
    	    exit_for_RtoL:
    	    	j merge_row_success
    	    
    merge_row_success:
    	move $v0,$t5
    	jr $ra
    merge_row_error:
    	li $v0,-1
    	jr $ra
################################################################
merge_col:
    #a0 -> board, a1 -> n_rows, a2 -> n_cols, a3 -> col, ($sp)-> direction  
    lw $t0,($sp)
    move $t5,$a1 #this is a counter of how many elements left
    bltz $a3,merge_col_error
    bge $a3,$a2,merge_col_error
    blt $a1,2,merge_col_error
    blt $a2,2,merge_col_error
    
    beqz $t0,col_BtoT
    beq $t0,1,col_TtoB
    j merge_col_error	        	        	        	    
    
    col_BtoT:
  	addi $t0,$a1,-1 #counter 1 ->row 1
    	addi $t1,$a1,-2 #counter 2 ->row 2
    	for_BtoT:
    	    addi $sp,$sp,-24
    	    sw $a1,($sp)
    	    sw $a2,4($sp)
    	    sw $a3,8($sp)
    	    sw $t0,12($sp)
    	    sw $t1,16($sp)
    	    sw $ra,20($sp)
    	    
    	    lw $a1,4($sp)
    	    move $a2,$t0
    	    lw $a3,8($sp)
    	    
    	    jal get_cell
    	    
    	    lw $a1,($sp)
    	    lw $a2,4($sp)
    	    lw $a3,8($sp)
    	    lw $t0,12($sp)
    	    lw $t1,16($sp)
    	    lw $ra,20($sp)
    	    addi $sp,$sp,24
    	    move $t8,$v0 #now the address of the first cell is store in $t8
    	    #########
    	    addi $sp,$sp,-24
    	    sw $a1,($sp)
    	    sw $a2,4($sp)
    	    sw $a3,8($sp)
    	    sw $t0,12($sp)
    	    sw $t1,16($sp)
    	    sw $ra,20($sp)
    	    
    	    lw $a1,4($sp)
    	    move $a2,$t1
    	    lw $a3,8($sp)
    	    
    	    jal get_cell
    	    
    	    lw $a1,($sp)
    	    lw $a2,4($sp)
    	    lw $a3,8($sp)
    	    lw $t0,12($sp)
    	    lw $t1,16($sp)
    	    lw $ra,20($sp)
    	    addi $sp,$sp,24
    	    move $t9,$v0 #the address of the second one is store at t9
    	    
    	    lh $t6,($t8)
    	    lh $t7,($t9) #t6,t7 now contains the number of each cell
    	    
    	    add $t4,$t6,$t7
    	    beq $t4,-2,cont_merge_BtoT
    	    
    	    beq $t6,$t7,merge_BtoT
    	    
    	    cont_merge_BtoT:
    	    addi $t0,$t0,-1
    	    addi $t1,$t1,-1
    	    bltz $t1,exit_for_BtoT
    	    j for_BtoT
    	    merge_BtoT:
    	    	li $t2,2
    	    	mul $t6,$t6,$t2
    	    	sh $t6,($t8)
    	    	li $t2,-1
    	    	sh $t2,($t9)
    	    	addi $t5,$t5,-1 #this is a counter of how many cells left
    	     	addi $t0,$t0,-1
    	    	addi $t1,$t1,-1
    	    	bltz $t1,exit_for_BtoT
    	    	j for_RtoL
    	    exit_for_BtoT:
    	    	j merge_col_success
  	   
    col_TtoB:
        li $t0,0
        li $t1,1
        for_TtoB:
            addi $sp,$sp,-24
    	    sw $a1,($sp) #n_rows
    	    sw $a2,4($sp) #n_cols
    	    sw $a3,8($sp) #cols
    	    sw $t0,12($sp)
    	    sw $t1,16($sp)
    	    sw $ra,20($sp)
    	    
    	    lw $a1,4($sp)
    	    move $a2,$t0
    	    lw $a3,8($sp)
    	    
    	    jal get_cell
    	    
    	    lw $a1,($sp)
    	    lw $a2,4($sp)
    	    lw $a3,8($sp)
    	    lw $t0,12($sp)
    	    lw $t1,16($sp)
    	    lw $ra,20($sp)
    	    addi $sp,$sp,24
    	    move $t8,$v0 #now the address of the first cell is store in $t8
    	    #########
    	    addi $sp,$sp,-24
    	    sw $a1,($sp)
    	    sw $a2,4($sp)
    	    sw $a3,8($sp)
    	    sw $t0,12($sp)
    	    sw $t1,16($sp)
    	    sw $ra,20($sp)
    	    
    	    lw $a1,4($sp)
    	    move $a2,$t1
    	    lw $a3,8($sp)
    	    
    	    jal get_cell
    	    
    	    lw $a1,($sp)
    	    lw $a2,4($sp)
    	    lw $a3,8($sp)
    	    lw $t0,12($sp)
    	    lw $t1,16($sp)
    	    lw $ra,20($sp)
    	    addi $sp,$sp,24  
      	    move $t9,$v0
      	    
      	    lh $t6,($t8)
    	    lh $t7,($t9) #t6,t7 now contains the number of each cell
    	    
    	    add $t4,$t6,$t7
    	    beq $t4,-2,cont_merge_TtoB
    	    
    	    beq $t6,$t7,merge_TtoB
    	    
    	    cont_merge_TtoB:
    	    addi $t0,$t0,1
    	    addi $t1,$t1,1
	    beq $t1,$a1,exit_for_TtoB
    	    j for_TtoB
    	    merge_TtoB:
    	    	li $t2,2
    	    	mul $t6,$t6,$t2
    	    	sh $t6,($t8)
    	    	li $t2,-1
    	    	sh $t2,($t9)
    	    	addi $t5,$t5,-1 #this is a counter of how many cells left
    	     	addi $t0,$t0,1
    	    	addi $t1,$t1,1
    	    	beq $t1,$a1,exit_for_TtoB
    	    	j for_TtoB
    	    exit_for_TtoB:
    	    	j merge_col_success
    
    merge_col_success:
    	move $v0,$t5
    	jr $ra
   	        	        	        	        	    	        	        	        	        	    	        	        	        	        	    
    merge_col_error:
    	li $v0,-1
    	jr $ra
       	        	        	        	        	        	        	     	        	        	        	        	        	        	    
    	    
#####################################################
shift_row:
    #a0 -> board, a1 -> n_rows, a2 -> n_cols, a3 -> row, ($sp)-> direction
    lw $t0,($sp)
    li $t5,0#t5 counter
    bltz $a3,shift_row_error
    bge $a3,$a1,shift_row_error
    blt $a1,2,shift_row_error
    blt $a2,2,shift_row_error
    
    beqz $t0,shift_LtoR
    beq $t0,1,shift_RtoL
    j shift_row_error	     

    shift_LtoR:
    	li $t0,0
    	for_shift_LtoR:
    	    addi $sp,$sp,-24
    	    sw $a1,($sp) #n_rows
    	    sw $a2,4($sp) #n_cols
    	    sw $a3,8($sp) #rows
    	    sw $t0,12($sp)
    	    sw $t1,16($sp)
    	    sw $ra,20($sp)
    	     
    	    lw $a1,4($sp)
    	    lw $a2,8($sp)
    	    move $a3,$t0
    	    
    	    jal get_cell
    	    
    	    lw $a1,($sp)
    	    lw $a2,4($sp)
    	    lw $a3,8($sp)
    	    lw $t0,12($sp)
    	    lw $t1,16($sp)
    	    lw $ra,20($sp)
    	    addi $sp,$sp,24
    	    move $t8,$v0 #now the address of the first cell is store in $t8
    	    lh $t6,($t8)
    	    
    	    beq $t6,-1,go_shift_LtoR
    	    cont_shift_LtoR:
    	    addi $t0,$t0,1
    	    
    	    addi $t4,$a2,-1
    	    beq $t0,$t4,exit_for_shift_LtoR
    	    j for_shift_LtoR
    	    go_shift_LtoR:
    	    	addi $t1,$t0,1
    	    	for_go_LtoR:
    	    	    addi $sp,$sp,-24
    	    	    sw $a1,($sp) #n_rows
    	   	    sw $a2,4($sp) #n_cols
    	            sw $a3,8($sp) #cols
    	            sw $t0,12($sp)
    	            sw $t1,16($sp)
    	            sw $ra,20($sp)
    	     
    	            lw $a1,4($sp)
    	    	    lw $a2,8($sp)
    	    	    move $a3,$t1
    	    
    	    	    jal get_cell
    	    
    	    	    lw $a1,($sp)
    	    	    lw $a2,4($sp)
    	    	    lw $a3,8($sp)
    	    	    lw $t0,12($sp)
    	    	    lw $t1,16($sp)
    	    	    lw $ra,20($sp)
    	    	    addi $sp,$sp,24
    	    	    move $t9,$v0
    	    	    lh $t7,($t9)
    	    	    bne $t7,-1,replace_LtoR
    	    	    addi $t1,$t1,1
    	    	    beq $t1,$a2,cont_shift_LtoR
    	    	    j for_go_LtoR
    	    	    replace_LtoR:
    	    	    	sh $t7,($t8)
    	    	    	li $t7,-1
    	    	    	sh $t7,($t9)
    	    	    	addi $t5,$t5,1
    	    	    	j cont_shift_LtoR
    	    	
    	    
    	    exit_for_shift_LtoR:
    	    	j shift_row_success
    	
    	
    shift_RtoL:
        addi $t0,$a2,-1
    	for_shift_RtoL:
    	    addi $sp,$sp,-24
    	    sw $a1,($sp) #n_rows
    	    sw $a2,4($sp) #n_cols
    	    sw $a3,8($sp) #cols
    	    sw $t0,12($sp)
    	    sw $t1,16($sp)
    	    sw $ra,20($sp)
    	     
    	    lw $a1,4($sp)
    	    lw $a2,8($sp)
    	    move $a3,$t0
    	    
    	    jal get_cell
    	    
    	    lw $a1,($sp)
    	    lw $a2,4($sp)
    	    lw $a3,8($sp)
    	    lw $t0,12($sp)
    	    lw $t1,16($sp)
    	    lw $ra,20($sp)
    	    addi $sp,$sp,24
    	    move $t8,$v0 #now the address of the first cell is store in $t8
    	    lh $t6,($t8)
    	    
    	    beq $t6,-1,go_shift_RtoL
    	    cont_shift_RtoL:
    	    addi $t0,$t0,-1
    	    beqz $t0,exit_for_shift_RtoL
    	    j for_shift_RtoL
    	    go_shift_RtoL:
    	    	addi $t1,$t0,-1
    	    	for_go_RtoL:
    	    	    addi $sp,$sp,-24
    	    	    sw $a1,($sp) #n_rows
    	   	    sw $a2,4($sp) #n_cols
    	            sw $a3,8($sp) #cols
    	            sw $t0,12($sp)
    	            sw $t1,16($sp)
    	            sw $ra,20($sp)
    	     
    	            lw $a1,4($sp)
    	    	    lw $a2,8($sp)
    	    	    move $a3,$t1
    	    
    	    	    jal get_cell
    	    
    	    	    lw $a1,($sp)
    	    	    lw $a2,4($sp)
    	    	    lw $a3,8($sp)
    	    	    lw $t0,12($sp)
    	    	    lw $t1,16($sp)
    	    	    lw $ra,20($sp)
    	    	    addi $sp,$sp,24
    	    	    move $t9,$v0
    	    	    lh $t7,($t9)
    	    	    bne $t7,-1,replace_RtoL
    	    	    addi $t1,$t1,-1
    	    	    bltz $t1,cont_shift_RtoL
    	    	    j for_go_RtoL
    	    	    replace_RtoL:
    	    	    	sh $t7,($t8)
    	    	    	li $t7,-1
    	    	    	sh $t7,($t9)
    	    	    	addi $t5,$t5,1
    	    	    	j cont_shift_RtoL
    	    
    	    exit_for_shift_RtoL:
    	    	j shift_row_success
     	
    shift_row_success:
    	move $v0,$t5
    	jr $ra
     	 	
    shift_row_error:
    	li $v0,-1
    	jr $ra
####################################################
shift_col:
    #a0 -> board, a1 -> n_rows, a2 -> n_cols, a3 -> col, ($sp)-> direction 
      
    lw $t0,($sp)
    li $t5,0#t5 counter
    bltz $a3,shift_col_error
    bge $a3,$a2,shift_col_error
    blt $a1,2,shift_col_error
    blt $a2,2,shift_col_error
    
    beqz $t0,shift_BtoT
    beq $t0,1,shift_TtoB
    j shift_col_error	
    
    
    shift_BtoT:
    	li $t0,0
    	for_shift_BtoT:
    	    addi $sp,$sp,-24
    	    sw $a1,($sp) #n_rows
    	    sw $a2,4($sp) #n_cols
    	    sw $a3,8($sp) #rows
    	    sw $t0,12($sp)
    	    sw $t1,16($sp)
    	    sw $ra,20($sp)
    	     
    	    lw $a1,4($sp)
    	    move $a2,$t0
    	    lw $a3,8($sp)
    	    
    	    jal get_cell
    	    
    	    lw $a1,($sp)
    	    lw $a2,4($sp)
    	    lw $a3,8($sp)
    	    lw $t0,12($sp)
    	    lw $t1,16($sp)
    	    lw $ra,20($sp)
    	    addi $sp,$sp,24
    	    move $t8,$v0 #now the address of the first cell is store in $t8
    	    lh $t6,($t8)
    	    
    	    beq $t6,-1,go_shift_BtoT
    	    cont_shift_BtoT:
    	    addi $t0,$t0,1
    	    
    	    addi $t4,$a2,-1
    	    beq $t0,$t4,exit_for_shift_BtoT
    	    j for_shift_BtoT
    	    go_shift_BtoT:
    	    	addi $t1,$t0,1
    	    	for_go_BtoT:
    	    	    addi $sp,$sp,-24
    	    	    sw $a1,($sp) #n_rows
    	   	    sw $a2,4($sp) #n_cols
    	            sw $a3,8($sp) #cols
    	            sw $t0,12($sp)
    	            sw $t1,16($sp)
    	            sw $ra,20($sp)
    	     
    	            lw $a1,4($sp)
    	    	    move $a2,$t1
    	    	    lw $a3,8($sp)
    	    
    	    	    jal get_cell
    	    
    	    	    lw $a1,($sp)
    	    	    lw $a2,4($sp)
    	    	    lw $a3,8($sp)
    	    	    lw $t0,12($sp)
    	    	    lw $t1,16($sp)
    	    	    lw $ra,20($sp)
    	    	    addi $sp,$sp,24
    	    	    move $t9,$v0
    	    	    lh $t7,($t9)
    	    	    bne $t7,-1,replace_BtoT
    	    	    addi $t1,$t1,1
    	    	    beq $t1,$a2,cont_shift_BtoT
    	    	    j for_go_BtoT
    	    	    replace_BtoT:
    	    	    	sh $t7,($t8)
    	    	    	li $t7,-1
    	    	    	sh $t7,($t9)
    	    	    	addi $t5,$t5,1
    	    	    	j cont_shift_BtoT
    	    	
    	    
    	    exit_for_shift_BtoT:
    	    	j shift_row_success
    	
    	
    shift_TtoB:
    addi $t0,$a1,-1
    	for_shift_TtoB:
    	    addi $sp,$sp,-24
    	    sw $a1,($sp) #n_rows
    	    sw $a2,4($sp) #n_cols
    	    sw $a3,8($sp) #cols
    	    sw $t0,12($sp)
    	    sw $t1,16($sp)
    	    sw $ra,20($sp)
    	     
    	    lw $a1,4($sp)
    	    move $a2,$t0
    	    lw $a3,8($sp)
    	    
    	    jal get_cell
    	    
    	    lw $a1,($sp)
    	    lw $a2,4($sp)
    	    lw $a3,8($sp)
    	    lw $t0,12($sp)
    	    lw $t1,16($sp)
    	    lw $ra,20($sp)
    	    addi $sp,$sp,24
    	    move $t8,$v0 #now the address of the first cell is store in $t8
    	    lh $t6,($t8)
    	    
    	    beq $t6,-1,go_shift_TtoB
    	    cont_shift_TtoB:
    	    addi $t0,$t0,-1
    	    beqz $t0,exit_for_shift_TtoB
    	    j for_shift_TtoB
    	    go_shift_TtoB:
    	    	addi $t1,$t0,-1
    	    	for_go_TtoB:
    	    	    addi $sp,$sp,-24
    	    	    sw $a1,($sp) #n_rows
    	   	    sw $a2,4($sp) #n_cols
    	            sw $a3,8($sp) #cols
    	            sw $t0,12($sp)
    	            sw $t1,16($sp)
    	            sw $ra,20($sp)
    	     
    	            lw $a1,4($sp)
    	    	    move $a2,$t1
    	            lw $a3,8($sp)
    	    
    	    	    jal get_cell
    	    
    	    	    lw $a1,($sp)
    	    	    lw $a2,4($sp)
    	    	    lw $a3,8($sp)
    	    	    lw $t0,12($sp)
    	    	    lw $t1,16($sp)
    	    	    lw $ra,20($sp)
    	    	    addi $sp,$sp,24
    	    	    move $t9,$v0
    	    	    lh $t7,($t9)
    	    	    bne $t7,-1,replace_TtoB
    	    	    addi $t1,$t1,-1
    	    	    bltz $t1,cont_shift_TtoB
    	    	    j for_go_TtoB
    	    	    replace_TtoB:
    	    	    	sh $t7,($t8)
    	    	    	li $t7,-1
    	    	    	sh $t7,($t9)
    	    	    	addi $t5,$t5,1
    	    	    	j cont_shift_TtoB
    	    
    	    exit_for_shift_TtoB:
    	    	j shift_col_success
    
    
    shift_col_success:
    	move $v0,$t5
    	jr $ra
    shift_col_error:
    	li $v0,-1
    	jr $ra
    
####################################################
#Part II - Game Play
check_state:
    #a0->board, a1->n_rows, a2 -> n_cols
    #first check if there is 2048, if there is then return 1,
    #second check there is any -1, if there is -1 then return 0
    #third check adjacent cells for same value, if not for all return -1, else return 0
    li $t0,0 #row
    for_check_state_2048:
    	li $t1,0 #col
    	for_check_state_2:
    	    addi $sp,$sp,-20
    	    sw $a1,($sp)
    	    sw $a2,4($sp)
    	    sw $ra,8($sp)
    	    sw $t0,12($sp)
    	    sw $t1,16($sp)
    	    
    	    lw $a1,4($sp)
    	    move $a2,$t0
    	    move $a3,$t1
    	    
    	    jal get_cell
    	    
    	    lw $a1,($sp)
    	    lw $a2,4($sp)
    	    lw $ra,8($sp)
    	    lw $t0,12($sp)
    	    lw $t1,16($sp)
    	    addi $sp,$sp,20
    	    move $t2,$v0
    	    lh $t2,($t2)
    	    beq $t2,2048,game_won
    	    addi $t1,$t1,1
    	    beq $t1,$a2,exit_for_check_state_2
    	    j for_check_state_2    
        exit_for_check_state_2:
            addi $t0,$t0,1
            beq $t0,$a1,exit_for_check_state_2048
            j for_check_state_2048
        exit_for_check_state_2048:
    ##################check for occurance of -1##################
    li $t0,0 #row
    for_check_state_negative:
    	li $t1,0 #col
    	for_check_state_3:
    	    addi $sp,$sp,-20
    	    sw $a1,($sp)
    	    sw $a2,4($sp)
    	    sw $ra,8($sp)
    	    sw $t0,12($sp)
    	    sw $t1,16($sp)
    	    
    	    lw $a1,4($sp)
    	    move $a2,$t0
    	    move $a3,$t1
    	    
    	    jal get_cell
    	    
    	    lw $a1,($sp)
    	    lw $a2,4($sp)
    	    lw $ra,8($sp)
    	    lw $t0,12($sp)
    	    lw $t1,16($sp)
    	    addi $sp,$sp,20
    	    move $t2,$v0
    	    lh $t2,($t2)
    	    beq $t2,-1,game_cont
    	    addi $t1,$t1,1
    	    beq $t1,$a2,exit_for_check_state_3
    	    j for_check_state_3    
        exit_for_check_state_3:
            addi $t0,$t0,1
            beq $t0,$a1,exit_for_check_state_negative
            j for_check_state_negative
        exit_for_check_state_negative:   
    ################################################  
    check_rows:
    	li $t0,0 #the row increment
    	
    	check_rows_start:
    	    li $t1,0 #col1
    	    li $t2,1 #col2
    	    for_check_rows:
    	        addi $sp,$sp,-24
    	    	sw $a1,($sp)
    	    	sw $a2,4($sp)
    	    	sw $ra,8($sp)
    	    	sw $t0,12($sp)
    	    	sw $t1,16($sp)
    	    	sw $t2,20($sp)
    	    	
    	    	lw $a1,4($sp)
    	    	move $a2,$t0
    	    	move $a3,$t1
    	    
    	    	jal get_cell    
    	    	
    	    	lw $a1,($sp)
    	    	lw $a2,4($sp)
    	    	lw $ra,8($sp)
    	    	lw $t0,12($sp)
    	    	lw $t1,16($sp)
    	    	lw $t2,20($sp)
    	    	addi $sp,$sp,24
    	    	move $t8,$v0
    	    	lh $t8,($t8)
    	    	
    	    	addi $sp,$sp,-24
    	    	sw $a1,($sp)
    	    	sw $a2,4($sp)
    	    	sw $ra,8($sp)
    	    	sw $t0,12($sp)
    	    	sw $t1,16($sp)
    	    	sw $t2,20($sp)
    	    	
    	    	lw $a1,4($sp)
    	    	move $a2,$t0
    	    	move $a3,$t2
    	    
    	    	jal get_cell    
    	    	
    	    	lw $a1,($sp)
    	    	lw $a2,4($sp)
    	    	lw $ra,8($sp)
    	    	lw $t0,12($sp)
    	    	lw $t1,16($sp)
    	    	lw $t2,20($sp)
    	    	addi $sp,$sp,24
    	    	move $t9,$v0
    	    	lh $t9,($t9)
    	    	
    	    	beq $t8,$t9,game_cont
    	    	addi $t1,$t1,1
    	    	addi $t2,$t2,1
    	    	beq $t2,$a2,exit_for_check_rows
    	    	
    	   exit_for_check_rows:
    	   	addi $t0,$t0,1
    	   	beq $t0,$a1,exit_check_rows
    	   	j check_rows_start
    	   exit_check_rows:
    
    check_cols:
    	li $t0,0 #the col increment
    	
    	check_cols_start:
    	    li $t1,0 #row1
    	    li $t2,1 #row2
    	    for_check_cols:
    	        addi $sp,$sp,-24
    	    	sw $a1,($sp)
    	    	sw $a2,4($sp)
    	    	sw $ra,8($sp)
    	    	sw $t0,12($sp)
    	    	sw $t1,16($sp)
    	    	sw $t2,20($sp)
    	    	
    	    	lw $a1,4($sp)
    	    	move $a2,$t1
    	    	move $a3,$t0
    	    
    	    	jal get_cell    
    	    	
    	    	lw $a1,($sp)
    	    	lw $a2,4($sp)
    	    	lw $ra,8($sp)
    	    	lw $t0,12($sp)
    	    	lw $t1,16($sp)
    	    	lw $t2,20($sp)
    	    	addi $sp,$sp,24
    	    	move $t8,$v0
    	    	lh $t8,($t8)
    	    	
    	    	addi $sp,$sp,-24
    	    	sw $a1,($sp)
    	    	sw $a2,4($sp)
    	    	sw $ra,8($sp)
    	    	sw $t0,12($sp)
    	    	sw $t1,16($sp)
    	    	sw $t2,20($sp)
    	    	
    	    	lw $a1,4($sp)
    	    	move $a2,$t2
    	    	move $a3,$t0
    	    
    	    	jal get_cell    
    	    	
    	    	lw $a1,($sp)
    	    	lw $a2,4($sp)
    	    	lw $ra,8($sp)
    	    	lw $t0,12($sp)
    	    	lw $t1,16($sp)
    	    	lw $t2,20($sp)
    	    	addi $sp,$sp,24
    	    	move $t9,$v0
    	    	lh $t9,($t9)
    	    	
    	    	beq $t8,$t9,game_cont
    	    	addi $t1,$t1,1
    	    	addi $t2,$t2,1
    	    	beq $t2,$a2,exit_for_check_cols
    	    	
    	   exit_for_check_cols:
    	   	addi $t0,$t0,1
    	   	beq $t0,$a1,exit_check_cols
    	   	j check_cols_start
    	   exit_check_cols:
    	   
    game_lose:
    	li $v0,-1
    	jr $ra		    	   
    game_cont:
    	li $v0,0
    	jr $ra	    
    game_won:
    	li $v0,1
    	jr $ra	    
##############################################
    
user_move:
    #a0 -> board, a1 -> n_rows, a2-> n_cols, a3-> dir
    beq $a3,'L',move_left
    beq $a3,'R',move_right
    beq $a3,'U',move_up
    beq $a3,'D',move_down
    
    user_move_error:
    	li $v0,-1
    	li $v1,-1
    	jr $ra
    move_left:
    	li $t0,0
    	for_move_left:
    	   ########shift_row
    	   addi $sp,$sp,-20
    	   sw $a1,($sp)
    	   sw $a2,4($sp) 
    	   sw $a3,8($sp)
    	   sw $t0,12($sp)
    	   sw $ra,16($sp)
    	   
    	   move $a3,$t0
    	   addi $sp,$sp,-4
    	   li $t0,0
    	   sw $t0,($sp)
    	   jal shift_row
    	   
    	   addi $sp,$sp,4
    	   lw $a1,($sp)
    	   lw $a2,4($sp) 
    	   lw $a3,8($sp)
    	   lw $t0,12($sp)
    	   lw $ra,16($sp)
    	   addi $sp,$sp,20
    	   beq $v0,-1,user_move_error
    	   #########merge_row###
    	   addi $sp,$sp,-20
    	   sw $a1,($sp)
    	   sw $a2,4($sp) 
    	   sw $a3,8($sp)
    	   sw $t0,12($sp)
    	   sw $ra,16($sp)
    	   
    	   move $a3,$t0
    	   addi $sp,$sp,-4
    	   li $t0,1
    	   sw $t0,($sp)
    	   jal merge_row
    	   
    	   addi $sp,$sp,4
    	   lw $a1,($sp)
    	   lw $a2,4($sp) 
    	   lw $a3,8($sp)
    	   lw $t0,12($sp)
    	   lw $ra,16($sp)
    	   addi $sp,$sp,20
    	   beq $v0,-1,user_move_error
    	   ########shift_row####
    	   addi $sp,$sp,-20
    	   sw $a1,($sp)
    	   sw $a2,4($sp) 
    	   sw $a3,8($sp)
    	   sw $t0,12($sp)
    	   sw $ra,16($sp)
    	   
    	   move $a3,$t0
    	   addi $sp,$sp,-4
    	   li $t0,0
    	   sw $t0,($sp)
    	   jal shift_row
    	   
    	   addi $sp,$sp,4
    	   lw $a1,($sp)
    	   lw $a2,4($sp) 
    	   lw $a3,8($sp)
    	   lw $t0,12($sp)
    	   lw $ra,16($sp)
    	   addi $sp,$sp,20
    	   beq $v0,-1,user_move_error
    	   addi $t0,$t0,1
    	   beq $t0,$a1,call_check_gamestate
    	   j for_move_left
    
    move_right:
    	li $t0,0
    	for_move_right:
    	   ########shift_row
    	   addi $sp,$sp,-20
    	   sw $a1,($sp)
    	   sw $a2,4($sp) 
    	   sw $a3,8($sp)
    	   sw $t0,12($sp)
    	   sw $ra,16($sp)
    	   
    	   move $a3,$t0
    	   addi $sp,$sp,-4
    	   li $t0,1
    	   sw $t0,($sp)
    	   jal shift_row
    	   
    	   addi $sp,$sp,4
    	   lw $a1,($sp)
    	   lw $a2,4($sp) 
    	   lw $a3,8($sp)
    	   lw $t0,12($sp)
    	   lw $ra,16($sp)
    	   addi $sp,$sp,20
    	   beq $v0,-1,user_move_error
    	   #########merge_row###
    	   addi $sp,$sp,-20
    	   sw $a1,($sp)
    	   sw $a2,4($sp) 
    	   sw $a3,8($sp)
    	   sw $t0,12($sp)
    	   sw $ra,16($sp)
    	   
    	   move $a3,$t0
    	   addi $sp,$sp,-4
    	   li $t0,0
    	   sw $t0,($sp)
    	   jal merge_row
    	   
    	   addi $sp,$sp,4
    	   lw $a1,($sp)
    	   lw $a2,4($sp) 
    	   lw $a3,8($sp)
    	   lw $t0,12($sp)
    	   lw $ra,16($sp)
    	   addi $sp,$sp,20
    	   beq $v0,-1,user_move_error
    	   ########shift_row####
    	   addi $sp,$sp,-20
    	   sw $a1,($sp)
    	   sw $a2,4($sp) 
    	   sw $a3,8($sp)
    	   sw $t0,12($sp)
    	   sw $ra,16($sp)
    	   
    	   move $a3,$t0
    	   addi $sp,$sp,-4
    	   li $t0,1
    	   sw $t0,($sp)
    	   jal shift_row
    	   
    	   addi $sp,$sp,4
    	   lw $a1,($sp)
    	   lw $a2,4($sp) 
    	   lw $a3,8($sp)
    	   lw $t0,12($sp)
    	   lw $ra,16($sp)
    	   addi $sp,$sp,20
    	   beq $v0,-1,user_move_error
    	   addi $t0,$t0,1
    	   beq $t0,$a1,call_check_gamestate
    	   j for_move_right
    move_up:
    	li $t0,0
    	for_move_up:
    	   ########shift_col
    	   addi $sp,$sp,-20
    	   sw $a1,($sp)
    	   sw $a2,4($sp) 
    	   sw $a3,8($sp)
    	   sw $t0,12($sp)
    	   sw $ra,16($sp)
    	   
    	   move $a3,$t0
    	   addi $sp,$sp,-4
    	   li $t0,0
    	   sw $t0,($sp)
    	   jal shift_col
    	   
    	   addi $sp,$sp,4
    	   lw $a1,($sp)
    	   lw $a2,4($sp) 
    	   lw $a3,8($sp)
    	   lw $t0,12($sp)
    	   lw $ra,16($sp)
    	   addi $sp,$sp,20
    	   beq $v0,-1,user_move_error
    	   #########merge_col###
    	   addi $sp,$sp,-20
    	   sw $a1,($sp)
    	   sw $a2,4($sp) 
    	   sw $a3,8($sp)
    	   sw $t0,12($sp)
    	   sw $ra,16($sp)
    	   
    	   move $a3,$t0
    	   addi $sp,$sp,-4
    	   li $t0,0
    	   sw $t0,($sp)
    	   jal merge_col
    	   
    	   addi $sp,$sp,4
    	   lw $a1,($sp)
    	   lw $a2,4($sp) 
    	   lw $a3,8($sp)
    	   lw $t0,12($sp)
    	   lw $ra,16($sp)
    	   addi $sp,$sp,20
    	   beq $v0,-1,user_move_error
    	   ########shift_col####
    	   addi $sp,$sp,-20
    	   sw $a1,($sp)
    	   sw $a2,4($sp) 
    	   sw $a3,8($sp)
    	   sw $t0,12($sp)
    	   sw $ra,16($sp)
    	   
    	   move $a3,$t0
    	   addi $sp,$sp,-4
    	   li $t0,0
    	   sw $t0,($sp)
    	   jal shift_col
    	   
    	   addi $sp,$sp,4
    	   lw $a1,($sp)
    	   lw $a2,4($sp) 
    	   lw $a3,8($sp)
    	   lw $t0,12($sp)
    	   lw $ra,16($sp)
    	   addi $sp,$sp,20
    	   beq $v0,-1,user_move_error
    	   addi $t0,$t0,1
    	   beq $t0,$a2,call_check_gamestate
    	   j for_move_up
    move_down:
    	li $t0,0
    	for_move_down:
    	   ########shift_col
    	   addi $sp,$sp,-20
    	   sw $a1,($sp)
    	   sw $a2,4($sp) 
    	   sw $a3,8($sp)
    	   sw $t0,12($sp)
    	   sw $ra,16($sp)
    	   
    	   move $a3,$t0
    	   addi $sp,$sp,-4
    	   li $t0,1
    	   sw $t0,($sp)
    	   jal shift_col
    	   
    	   addi $sp,$sp,4
    	   lw $a1,($sp)
    	   lw $a2,4($sp) 
    	   lw $a3,8($sp)
    	   lw $t0,12($sp)
    	   lw $ra,16($sp)
    	   addi $sp,$sp,20
    	   beq $v0,-1,user_move_error
    	   #########merge_col###
    	   addi $sp,$sp,-20
    	   sw $a1,($sp)
    	   sw $a2,4($sp) 
    	   sw $a3,8($sp)
    	   sw $t0,12($sp)
    	   sw $ra,16($sp)
    	   
    	   move $a3,$t0
    	   addi $sp,$sp,-4
    	   li $t0,1
    	   sw $t0,($sp)
    	   jal merge_col
    	   
    	   addi $sp,$sp,4
    	   lw $a1,($sp)
    	   lw $a2,4($sp) 
    	   lw $a3,8($sp)
    	   lw $t0,12($sp)
    	   lw $ra,16($sp)
    	   addi $sp,$sp,20
    	   beq $v0,-1,user_move_error
    	   ########shift_col####
    	   addi $sp,$sp,-20
    	   sw $a1,($sp)
    	   sw $a2,4($sp) 
    	   sw $a3,8($sp)
    	   sw $t0,12($sp)
    	   sw $ra,16($sp)
    	   
    	   move $a3,$t0
    	   addi $sp,$sp,-4
    	   li $t0,1
    	   sw $t0,($sp)
    	   jal shift_col

    	   addi $sp,$sp,4
    	   lw $a1,($sp)
    	   lw $a2,4($sp) 
    	   lw $a3,8($sp)
    	   lw $t0,12($sp)
    	   lw $ra,16($sp)
    	   addi $sp,$sp,20
    	   beq $v0,-1,user_move_error
    	   
    	   addi $t0,$t0,1
    	   beq $t0,$a2,call_check_gamestate
    	   j for_move_down
    	   	    	
    call_check_gamestate:
    	addi $sp,$sp,-4
    	sw $ra($sp)
    	jal check_state
    	lw $ra($sp)
    	addi $sp,$sp,4	    			
    user_move_success:
    	move $v1,$v0
    	li $v0,0
    	jr $ra 
                                
