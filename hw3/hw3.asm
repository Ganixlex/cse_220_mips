##############################################################
# Homework #3
# name: Xiang Li
# sbuid: 110445032
##############################################################

.macro print_nn
    la $a0,double_newline
    li $v0,4
    syscall
.end_macro

.text

##############################
#HW 2 FUNCTIONS
##############################

replace1st:
    #Define your code here
	############################################
	#a0,a1,a2 as inputs
	check_range:
	    blt $a1,0x00,return_error1
	    bgt $a1,0x7F,return_error1
	    blt $a2,0x00,return_error1
	    bgt $a2,0x7F,return_error1
	
	loop_through_string:
	    lb $t0,($a0)
	    beq $t0,$a1,replace_string
	    beqz $t0,return_zero_replacement
	    addi $a0,$a0,1
	    j loop_through_string
	    
	    replace_string:
	        sb $a2,($a0)
	        j return_successful1	   
	    
	    
	return_successful1:
	    move $v0,$a0
	    addi $v0,$v0,1
	    jr $ra
	    
	return_zero_replacement:
	    li $v0,0
	    jr $ra
	
	return_error1:
	    li $v0,-1
	    jr $ra
	############################################
	

printStringArray:
    #Define your code here
	############################################
	#a0,a1,a2,a3 as inputs
	check_error:
	    blt $a3,1,return_error2
	    bltz $a1,return_error2
	    bltz $a2,return_error2
	    bge $a1,$a3,return_error2
	    bge $a2,$a3,return_error2
	    blt $a2,$a1,return_error2
	
	cal_string_printed: #t9 is a counter for the number of string printed
	    sub $t9,$a2,$a1
	    addi $t9,$t9,1
	    move $t8,$t9  #keep an extra copy of the string number
	    
	intialize_string_index:
	    li $t0,4
	    mul $t0,$t0,$a1
	    add $a0,$a0,$t0 #initialize a0 to the start address
	    
	print_string_loop:
            addi $sp,$sp,-4
            sw $a0,($sp)
            
            lw $t0,($a0)
            move $a0,$t0
	    li $v0,4
	    syscall
	    
	    print_nn()
	    addi $t9,$t9,-1
	    lw $a0,($sp)
	    addi $sp,$sp,4
	    beqz $t9,return_successful2
	    
	    
	    addi $a0,$a0,4
	    j print_string_loop
	    
	return_successful2:
	    move $v0,$t8
	    jr $ra
	    
	return_error2:
	    li $v0,-1
	    jr $ra

	############################################
    
verifyIPv4Checksum:
    #Define your code here
	############################################
	#a0 as the input
	li $t0,0
	li $t1,0
	li $t2,0
	li $t3,0
	li $t4,0
	get_Header_length:
	    lb $t0,3($a0)
	    sll $t0,$t0,28
	    srl $t0,$t0,28
	    #t0 now has the header length we can use it as a counter
	
	    li $t4,0x0000ffff
	adding_loop:
	    lh $t1,($a0)
	    and $t1,$t1,$t4
	    add $t3,$t3,$t1
	    addi $a0,$a0,2
	    
	    lh $t2,($a0)
	    and $t2,$t2,$t4
	    add $t3,$t3,$t2
	    addi $a0,$a0,2
	    
	    addi $t0,$t0,-1
	    beqz $t0,finish_adding
	    j adding_loop
	finish_adding:
	#now $t3 has the total value we need to compare it to 2^16
	compare_with2to16:
	    li $t0,0x00010000
	    bge $t3,$t0,end_around_carry
	    j flip_bits
	end_around_carry:
	    srl $t1,$t3,16
	    sll $t3,$t3,16
	    srl $t3,$t3,16
	    add $t3,$t3,$t1
	flip_bits:
	    li $t2,0x0000ffff
	    xor $t2,$t3,$t2 #t2 now has the inverted checksum
	    beqz $t2,return_zero
	return_calculated_checksum:
	    move $v0,$t2
	    jr $ra
	return_zero:
	    li $v0,0
	    jr $ra
	############################################
extractData:
    #Define your code here
	############################################
	#a0,a1,a2 are used as input
	li $t0,0 #this is an index to the parray
	li $t9,0 #we will set this as the number of bits that is added 
	
	loop_parray:
	    li $t1,60
	    addi $sp,$sp,-4
	    sw $a0,($sp)
	    
	    mul $t2,$t1,$t0
	    add $a0,$a0,$t2 #a0 is initialized in its position
	    
	   
	    checksum_verification:
	    
	    	#store the previous numbers into stack
	        addi $sp,$sp,-16
	        sw $a0,($sp)
	        sw $t0,4($sp)
	        sw $t2,8($sp)
	        sw $ra,12($sp)
	        
	        #call the IPv4 method
	        move $a0,$a0
	        li $t0,0
	        li $t1,0
	        li $t2,0
	        li $t3,0
	        li $t4,0
	        jal verifyIPv4Checksum
	        
	        #restore the stacks
	        lw $a0,($sp)
	        lw $t0,4($sp)
	        lw $t2,8($sp)
	        lw $ra,12($sp)
	        addi $sp,$sp,16
	        
	       	bnez $v0,return_error3
	       	
	    get_payload_length:
	        lh $t3,($a0)
	        andi $t3,$t3,0x0000ffff #t3 now has the total length
	        
	        li $t4,20
	        sub $t3,$t3,$t4 #t3 now has the payload length
	        add $t9,$t9,$t3
	        
	    
	    addi $a0,$a0,20 #skip the header field  
	    li $t4,0 #this is a counter      
	    write_into_msg: #a loop that allows me to write into the msg
	    	lb $t5,($a0)
	    	sb $t5,($a2)
	    	addi $a0,$a0,1
	    	addi $a2,$a2,1
	    	addi $t4,$t4,1
	    	bge $t4,$t3,done_write
	    	j write_into_msg
	   done_write:
	        ###
	        li $t5,10
	        sb $t5,($a2)
	    ###
	    
	    
	    
	    addi $t0,$t0,1
	    lw $a0,($sp)
	    addi $sp,$sp,4
	    bge $t0,$a1,exist_loop_parray
	    
	    j loop_parray
	    
	exist_loop_parray:
	    li $v0,0
	    move $v1,$t9
	    jr $ra
	
	return_error3:
	    lw $a0,($sp)
	    addi $sp,$sp,4
	    li $v0,-1
	    move $v1,$t0
	    jr $ra
	
	
	############################################
    

processDatagram:
    #Define your code here
    #a0,a1,a2 are inputs
    check_M_condition:
        blez $a1,return_error4
        
    li $t0,0 #this will be a index counter
    
    store_words:
        blez $a1,finish_datagram_loop
    	sw $a0,($a2) #store the address into sarray
    	addi $a2,$a2,4	
    	addi $t0,$t0,1
    datagram_loop:
        lb $t1,($a0)
        
        blez $a1,finish_datagram_loop
        addi $a1,$a1,-1
        bne $t1,10,j_loop
        replace_byte:
            
            addi $sp,$sp,-20
            sw $a0,($sp)
            sw $a1,4($sp)
            sw $a2,8($sp)
            sw $t0,12($sp)
            sw $ra,16($sp)
            
            move $a0,$a0
            li $a1,10
            li $a2,0
            jal replace1st
            
            lw $a0,($sp)
            lw $a1,4($sp)
            lw $a2,8($sp)
            lw $t0,12($sp)
            lw $ra,16($sp)
            addi $sp,$sp,20
            
            addi $a0,$a0,1
            j store_words
            
        
        
        j_loop:
        addi $a0,$a0,1
        j datagram_loop
    finish_datagram_loop:
    	#addi $a0,$a0,1
    	li $t1,0
    	sb $t1,($a0)
    	move $v0,$t0
    	jr $ra
    return_error4:
        li $v0,-1
        jr $ra

printDatagram:
    #Define your code here
    ############################################
    #a0,a1,a2,a3 are used as input
    check_n_size:
    	blez $a1,return_error5
    	
    check_valid_checksum:
        addi $sp,$sp,-20
        sw $a0,($sp)
        sw $a1,4($sp)
        sw $a2,8($sp)
        sw $a3,12($sp)
       	sw $ra,16($sp)
        
        jal extractData
        
        lw $a0,($sp)
        lw $a1,4($sp)
        lw $a2,8($sp)
        lw $a3,12($sp)
        lw $ra,16($sp)
        addi $sp,$sp,20
        
        #the msg is modified, and v1 has the number of bytes in the msg
        #now check v0 for 0 if all the checksum are valid
        bnez $v0,return_error5
    calling_ProcessDatagram:
    
    	move $a0,$a2
        move $a1,$v1
        move $a2,$a3
        
        
        
        addi $sp,$sp,-8
        sw $ra,($sp)
        sw $a2,4($sp)
       
        
        jal processDatagram
        
        lw $ra,($sp)
        lw $a2,4($sp)
        addi $sp,$sp,8
    	
    	#we only need the msg array to print it, which is a2
    	#v0 is the index end +1 and the length of the array
    	bltz $v0,return_error5
    	
    print_the_string:
    	addi $sp,$sp,-4
    	sw $ra,($sp)
    	
    	move $a0,$a2
    	li $a1,0
    	move $a3,$v0
    	addi $a2,$v0,-1
    	
    	jal printStringArray
    	
    	lw $ra,($sp)
    	addi $sp,$sp,4
    	
    	bltz $v0,return_error5
    	
    return_success5:
        li $v0,0
        jr $ra
    	
    return_error5:
    	li $v0,-1
    	jr $ra
###########################
#HW 3 functions
##########################

extractUnorderedData:
    #a0 - parray,a1 - n,a2 - msg,a3 -packetentrysize
    #this first check if n is less then 1
    blt $a1,1,return_1_1_error
    
    ###################################Doing Checksum and Total Length Verification##########################
    li $t0,0 #this is the index to the parrayUO
    loop_parrayUO_for_checksum:
    	#the packet size is in $a3
    	addi $sp,$sp,-4
    	sw $a0,($sp) #store the initial address of the array into the sp

    	mul $t2,$t0,$a3 #let t2 be the offset 
    	add $a0,$a0,$t2 #now a0 is initialized at the start of each index
    	checksum_verificationUO:
    	    
    	    #store the previous inputs in the stack
    	    addi $sp,$sp,-20
    	    sw $a0,($sp)
    	    sw $t0,4($sp)
    	    sw $t2,8($sp)
    	    sw $a3,12($sp)
    	    sw $ra,16($sp)
    	    
    	    #call the verifying method
    	    move $a0,$a0
    	    li $t0,0
    	    li $t1,0
    	    li $t2,0
    	    li $t3,0
    	    li $t4,0
    	    jal verifyIPv4Checksum
    	    
    	    #restore the stacks
    	    lw $a0,($sp)
    	    lw $t0,4($sp)
    	    lw $t2,8($sp)
    	    lw $a3,12($sp)
    	    lw $ra,16($sp)
    	    addi $sp,$sp,20
    	    
    	    bnez $v0,return_checksum_error
    	    
    	    
    	check_total_length_field:
    	    #now a0 is still the start of a packet
    	    lh $t1,($a0)
    	    andi $t1,$t1,0x0000ffff #now t1 has the total length number
    	    
    	    bgt $t1,$a3,return_checksum_error   
    	    
        addi $t0,$t0,1
        lw $a0,($sp)
        addi $sp,$sp,4
        bge $t0,$a1,exit_loop_parrayUO_for_checksum
        j loop_parrayUO_for_checksum 
        
    exit_loop_parrayUO_for_checksum:
    # if this is reached, then checksum and packetsize is ok
    ########################################Check for n>1 and n == 1###################################
    #now check for n==1 or n >1
    beq $a1,1,for_n_equalone
    bgt $a1,1,for_n_greaterone
    for_n_equalone:
    	
    	#a0 is the starting address of the only packet
    	
  	lb $t0,5($a0)
  	srl $t0,$t0,5
    	sll $t0,$t0,29
    	srl $t0,$t0,29
    	#this will get us the flag element
    	
    	beqz $t0,for_000
    	check_010:
    	    bne $t0,0x00000002,return_1_1_error #check if the flag is not equal to 0000 0000 0000 0000 0000 0000 0000 0010
    	    j finish_n_equalone
    	for_000:
    	    #check_fragments
    	    lb $t1,5($a0)
    	    sll $t1,$t1,27
    	    sra $t1,$t1,19
    	    
    	    lb $t2,4($a0)
    	    li $t3,0x000000ff
    	    and $t2,$t2,$t3
    	    or $t1,$t1,$t2
    	    #t1 now contains the fragment offset
    	    bnez $t1,return_1_1_error
    	finish_n_equalone:     
    	    j finish_n_check
    	
    for_n_greaterone:
    	#now we have to loop through the packets to check for n>1
    	li $t0,0
    	addi $sp,$sp,-8
    	sw $s0,($sp) #this is a way of checking if there is only one element, we will 
    	sw $s1,4($sp) #add one each time a 100 or 000 shows up
    	li $s0,0
    	li $s1,0
    	loop_n_greaterone:
    	    addi $sp,$sp,-4
    	    sw $a0,($sp) #store the initial address of the array into the sp
    	    mul $t1,$t0,$a3 #let t1 be the offset of address
    	    add $a0,$a0,$t1 #now a0 is initialized at the start of each index
    	    
    	    #get flags
    	    lb $t2,5($a0)
  	    srl $t2,$t2,5
    	    sll $t2,$t2,29
    	    srl $t2,$t2,29
    	    beq $t2,0x00000002,return_1_1_error
    	    beq $t2,0x00000004,offset_1
    	    beq $t2,0x00000000,offset_2
    	    j f_offset
    	    offset_1:
    	    	 #check_fragments
    	    	lb $t1,5($a0)
    	    	sll $t1,$t1,27
    	    	sra $t1,$t1,19
    	    
    	    	lb $t2,4($a0)
    	    	li $t3,0x000000ff
    	    	and $t2,$t2,$t3
    	    	or $t1,$t1,$t2
    	    	#t1 now contains the fragment offset
    	    	beqz $t1,add_start
    	    	addi $t0,$t0,1
    	    	lw $a0,($sp)
    	    	addi $sp,$sp,4
    	    	bge $t0,$a1,f_offset
    	    	j loop_n_greaterone
    	    	add_start:
    	    	    addi $s0,$s0,1
    	    	    addi $t0,$t0,1
    	    	    lw $a0,($sp)
    	    	    addi $sp,$sp,4
    	    	    bge $t0,$a1,f_offset
    	    	    j loop_n_greaterone
    	    offset_2:
    	    	 #check_fragments
    	    	lb $t1,5($a0)
    	    	sll $t1,$t1,27
    	    	sra $t1,$t1,19
    	    
    	    	lb $t2,4($a0)
    	    	li $t3,0x000000ff
    	    	and $t2,$t2,$t3
    	    	or $t1,$t1,$t2
    	    	#t1 now contains the fragment offset
    	    	bnez $t1,add_end
    	    	addi $t0,$t0,1
    	    	lw $a0,($sp)
    	    	addi $sp,$sp,4
    	    	bge $t0,$a1,f_offset
    	    	j loop_n_greaterone
    	    	add_end:
    	    	    addi $s1,$s1,1
    	    	    addi $t0,$t0,1
    	    	    lw $a0,($sp)
    	    	    addi $sp,$sp,4
    	    	    bge $t0,$a1,f_offset
    	    	    j loop_n_greaterone
    	f_offset:
    	    bne $s0,1,return_with_error_s
    	    bne $s1,1,return_with_error_s
    	    lw $s0,($sp) 
    	    lw $s1,4($sp)
    	    addi $sp,$sp,8
    finish_n_check: 
    #if this is reached, then n conditions are good
    ##############################Store the payload into msg###############################
    #now we store it into the msg
    store_into_msg:
    	#a2 is the address of the msg
    	#we need to check the flag for where we store them, first we loop through the packets
    	li $t0,0 #this is an index counter
    	li $t9,0 #this counts all the bits
    	loop_msg:
	    addi $sp,$sp,-8
    	    sw $a0,($sp) #store the initial address of the array into the sp
    	    sw $a2,4($sp)
    	    mul $t2,$t0,$a3 #let t2 be the offset 
    	    add $a0,$a0,$t2 #now a0 is initialized at the start of each index
    	    
    	    lb $t1,5($a0)
  	    srl $t1,$t1,5
    	    sll $t1,$t1,29
    	    srl $t1,$t1,29
    	    #extract the flag
    	    
    	    
    	    #so for flag 100 or 000 if the fragment offset is 0 it will store at 0, if offset is not 0 it will store at offset
    	    beq $t1,0x00000002,store_010
    	    #get offsets
    	    #check_fragments
    	    lb $t1,5($a0)
    	    sll $t1,$t1,27
    	    sra $t1,$t1,19
    	    
    	    lb $t2,4($a0)
    	    andi $t2,$t2,0x000000ff
    	    or $t1,$t1,$t2
    	    #t1 now contains the fragment offset
    	    
    	    #move a2 to the offset
    	    add $a2,$a2,$t1
    	    
    	    #move the a0 to the payload start
    	    get_header_lengthUO:
    	    	lb $t6,3($a0)
    	    	andi $t6,$t6,0x0000000f #this gets us the header length
    	    	li $t8,4
    	    	mul $t6,$t6,$t8
    	    	
    	    get_payload_lengthUO:
	        lh $t3,($a0)
	        add $a0,$a0,$t6
	        andi $t3,$t3,0x0000ffff #t3 now has the total length
	        sub $t3,$t3,$t6 #t3 now has the payload length
	        add $t9,$t9,$t3
  
    	    li $t4,0
    	    write_into_msgUO: #a loop that allows me to write into the msg
	    	lb $t5,($a0)
	    	sb $t5,($a2)
	    	addi $a0,$a0,1
	    	addi $a2,$a2,1
	    	addi $t4,$t4,1
	    	bge $t4,$t3,done_writeUO
	    	j write_into_msgUO
	    done_writeUO:
	        
	    	j finish_writeUO
	    #############if flag=010##############
    	    store_010:
    	    	j get_header_lengthUO
    	    finish_writeUO:
    	    	
    	    addi $t0,$t0,1
            lw $a0,($sp)
            lw $a2,4($sp)
            addi $sp,$sp,8
            bge $t0,$a1,exit_loop_msg
            j loop_msg
	
	exit_loop_msg:


    return_UOsuccessful:
        li $v0,0
        move $v1,$t9
        jr $ra
    	    
    return_checksum_error:
    	lw $a0,($sp)
	addi $sp,$sp,4
	li $v0,-1
	move $v1,$t0
	jr $ra
	
    return_with_error_s:
        lw $s0,($sp) 
    	lw $s1,4($sp)
    	addi $sp,$sp,8
    	li $v0,-1
    	li $v1,-1
    	jr $ra
    	
    return_1_1_error:
    	li $v0,-1
    	li $v1,-1
    	jr $ra	   	
    	
printUnorderedDatagram:
    #a0-> parray ,a1 -> n,a2 -> msg,a3 -> sarray, and 0($sp) -> packetentrysize
    lw $t0,($sp)
    check_n_sizeUO:
    	blez $a1,return_errorUO
    	
    check_valid_checksumUO:
    
        addi $sp,$sp,-20
        sw $a0,($sp)
        sw $a1,4($sp)
        sw $a2,8($sp)
        sw $a3,12($sp)
       	sw $ra,16($sp)
       	
        move $a3,$t0
        jal extractUnorderedData
        
        lw $a0,($sp)
        lw $a1,4($sp)
        lw $a2,8($sp)
        lw $a3,12($sp)
        lw $ra,16($sp)
        addi $sp,$sp,20
        
        #the msg is modified, and v1 has the number of bytes in the msg
        #now check v0 for 0 if all the checksum are valid
        bnez $v0,return_errorUO
    calling_ProcessDatagramIUO:
    
    	move $a0,$a2
        move $a1,$v1
        move $a2,$a3
        
        
        
        addi $sp,$sp,-8
        sw $ra,($sp)
        sw $a2,4($sp)
       
        
        jal processDatagram
        
        lw $ra,($sp)
        lw $a2,4($sp)
        addi $sp,$sp,8
    	
    	#we only need the msg array to print it, which is a2
    	#v0 is the index end +1 and the length of the array
    	bltz $v0,return_errorUO
    	
    print_the_stringUO:
    	addi $sp,$sp,-4
    	sw $ra,($sp)
    	
    	move $a0,$a2
    	li $a1,0
    	move $a3,$v0
    	addi $a2,$v0,-1
    	
    	jal printStringArray
    	
    	lw $ra,($sp)
    	addi $sp,$sp,4
    	
    	bltz $v0,return_errorUO
    	
    return_successUO:
        li $v0,0
        jr $ra
    	
    return_errorUO:
    	li $v0,-1
    	jr $ra  	
#########################Recursive Function###################
#edit distance takes in 4 variables
#a0 -> str1, a1 -> str2, a2 -> m, a3 -> n
editDistance:
    addi $sp,$sp,-20
    sw $a0,($sp)
    sw $a1,4($sp)
    sw $a2,8($sp)
    sw $a3,12($sp)
    sw $ra,16($sp)
    blt $a2,0,return_negative
    blt $a3,0,return_negative
    addi $sp,$sp,20
    ##########system out print############
    addi $sp,$sp,-4
    sw $a0,($sp)
    
    la $a0,m
    li $v0,4
    syscall
    move $a0,$a2
    li $v0,1
    syscall
    
    la $a0,n
    li $v0,4
    syscall
    move $a0,$a3
    li $v0,1
    syscall
    
    la $a0,newline
    li $v0,4
    syscall
    
    lw $a0,($sp)
    addi $sp,$sp,4
    #########if first or second string is empty##########
    addi $sp,$sp,-20
    sw $a0,($sp)
    sw $a1,4($sp)
    sw $a2,8($sp)
    sw $a3,12($sp)
    sw $ra,16($sp)  
  
    beqz $a2,return_n
    beqz $a3,return_m
    addi $sp,$sp,20
    ########if the last chars are same############
    
    addi $sp,$sp,-20
    sw $a0,($sp)
    sw $a1,4($sp)
    sw $a2,8($sp)
    sw $a3,12($sp)
    sw $ra,16($sp)
    
    addi $a2,$a2,-1
    addi $a3,$a3,-1
    add $a0,$a0,$a2
    add $a1,$a1,$a3
    lb $t0,($a0)
    lb $t1,($a1)
    beq $t0,$t1,same_last
    lw $a0,($sp)
    lw $a1,4($sp)
    lw $a2,8($sp)
    lw $a3,12($sp)
    lw $ra,16($sp)
    addi $sp,$sp,20
    ######################
    insert:
    	addi $sp,$sp,-24
    	sw $a0,($sp)
    	sw $a1,4($sp)
    	sw $a2,8($sp)
    	sw $a3,12($sp)
    	sw $ra,16($sp)
    	sw $s0,20($sp)
    	addi $a3,$a3,-1
    	
    	jal editDistance
    	
    	lw $a0,($sp)
    	lw $a1,4($sp)
    	lw $a2,8($sp)
    	lw $a3,12($sp)
    	lw $ra,16($sp)
    	lw $s0,20($sp)
    	addi $sp,$sp,24
    	move $s0,$v0 #insert is store at $s0
    remove:
    	addi $sp,$sp,-28
    	sw $a0,($sp)
    	sw $a1,4($sp)
    	sw $a2,8($sp)
    	sw $a3,12($sp)
    	sw $ra,16($sp)	
    	sw $s0,20($sp)
    	sw $s1,24($sp)
    	addi $a2,$a2,-1
    	jal editDistance
    	lw $a0,($sp)
    	lw $a1,4($sp)
    	lw $a2,8($sp)
    	lw $a3,12($sp)
    	lw $ra,16($sp)
    	lw $s0,20($sp)
    	lw $s1,24($sp)
    	addi $sp,$sp,28
    	move $s1,$v0 #remove is store at $s1
    	
    replace:
    	addi $sp,$sp,-32
    	sw $a0,($sp)
    	sw $a1,4($sp)
    	sw $a2,8($sp)
    	sw $a3,12($sp)
    	sw $ra,16($sp)
    	sw $s0,20($sp)
    	sw $s1,24($sp)
    	sw $s2,28($sp)	
    	addi $a2,$a2,-1
    	addi $a3,$a3,-1
    	
    	jal editDistance
    	lw $a0,($sp)
    	lw $a1,4($sp)
    	lw $a2,8($sp)
    	lw $a3,12($sp)
    	lw $ra,16($sp)
    	lw $s0,20($sp)
    	lw $s1,24($sp)
    	lw $s2,28($sp)
    	addi $sp,$sp,32
    	move $s2,$v0 #replace is store at $t9
    	
    
    ################compare those 3 number($s0,$s1,$s2)##########
    
    comparet89:
    	blt $s1,$s2,smaller89
    	move $t6,$s2
    	j compare67
    smaller89:
    	move $t6,$s1
    compare67:
    	blt $t6,$s0,smaller67
    	move $t6,$s0
    	j f_compare
    smaller67:
    	move $t6,$t6
    f_compare:
    	move $v0,$t6
    	addi $v0,$v0,1
    jr $ra

same_last:
    lw $a0,($sp)
    lw $a1,4($sp)
    jal editDistance
    lw $ra,16($sp)
    addi $sp,$sp,20
    jr $ra
                                       
return_n:
    lw $ra,16($sp)
    addi $sp,$sp,20
    move $v0,$a3
    jr $ra
return_m:
    lw $ra,16($sp)
    addi $sp,$sp,20
    move $v0,$a2
    jr $ra                            
                                    
return_negative:
    lw $ra,16($sp)
    addi $sp,$sp,20
    li $v0,-1
    jr $ra        
    

#################################################################
# Student defined data section
#################################################################
.data
.align 2  # Align next items to word boundary
double_newline: .asciiz "\n\n"
m: .asciiz "m:"
n: .asciiz ",n:"
newline: .asciiz "\n"
#place all data declarations here


