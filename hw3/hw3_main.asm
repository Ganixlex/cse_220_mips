
.include "hw3_examples.asm"

.data

word1: .asciiz "SuN"
word2: .asciiz "Mon"
.text

start_test:
    la $a0,word1
    la $a1,word2
    li $a2,3
    li $a3,3
    jal editDistance
    
    move $a0,$v0
    li $v0,1
    syscall

li $v0,10
syscall
.include "hw3.asm"
