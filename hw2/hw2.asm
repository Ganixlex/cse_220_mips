
##############################################################
# Homework #2
# name: Xiang Li
# sbuid: 110445032
##############################################################

.macro print_nn
    la $a0,double_newline
    li $v0,4
    syscall
.end_macro

.text

##############################
# PART 1 FUNCTIONS
##############################

replace1st:
    #Define your code here
	############################################
	#a0,a1,a2 as inputs
	check_range:
	    blt $a1,0x00,return_error1
	    bgt $a1,0x7F,return_error1
	    blt $a2,0x00,return_error1
	    bgt $a2,0x7F,return_error1
	
	loop_through_string:
	    lb $t0,($a0)
	    beq $t0,$a1,replace_string
	    beqz $t0,return_zero_replacement
	    addi $a0,$a0,1
	    j loop_through_string
	    
	    replace_string:
	        sb $a2,($a0)
	        j return_successful1	   
	    
	    
	return_successful1:
	    move $v0,$a0
	    addi $v0,$v0,1
	    jr $ra
	    
	return_zero_replacement:
	    li $v0,0
	    jr $ra
	
	return_error1:
	    li $v0,-1
	    jr $ra
	############################################
	

printStringArray:
    #Define your code here
	############################################
	#a0,a1,a2,a3 as inputs
	check_error:
	    blt $a3,1,return_error2
	    bltz $a1,return_error2
	    bltz $a2,return_error2
	    bge $a1,$a3,return_error2
	    bge $a2,$a3,return_error2
	    blt $a2,$a1,return_error2
	
	cal_string_printed: #t9 is a counter for the number of string printed
	    sub $t9,$a2,$a1
	    addi $t9,$t9,1
	    move $t8,$t9  #keep an extra copy of the string number
	    
	intialize_string_index:
	    li $t0,4
	    mul $t0,$t0,$a1
	    add $a0,$a0,$t0 #initialize a0 to the start address
	    
	print_string_loop:
            addi $sp,$sp,-4
            sw $a0,($sp)
            
            lw $t0,($a0)
            move $a0,$t0
	    li $v0,4
	    syscall
	    
	    print_nn()
	    addi $t9,$t9,-1
	    lw $a0,($sp)
	    addi $sp,$sp,4
	    beqz $t9,return_successful2
	    
	    
	    addi $a0,$a0,4
	    j print_string_loop
	    
	return_successful2:
	    move $v0,$t8
	    jr $ra
	    
	return_error2:
	    li $v0,-1
	    jr $ra

	############################################
    
verifyIPv4Checksum:
    #Define your code here
	############################################
	#a0 as the input
	get_Header_length:
	    lb $t0,3($a0)
	    sll $t0,$t0,28
	    srl $t0,$t0,28
	    #t0 now has the header length we can use it as a counter
	
	    li $t4,0x0000ffff
	adding_loop:
	    lh $t1,($a0)
	    and $t1,$t1,$t4
	    add $t3,$t3,$t1
	    addi $a0,$a0,2
	    
	    lh $t2,($a0)
	    and $t2,$t2,$t4
	    add $t3,$t3,$t2
	    addi $a0,$a0,2
	    
	    addi $t0,$t0,-1
	    beqz $t0,finish_adding
	    j adding_loop
	finish_adding:
	#now $t3 has the total value we need to compare it to 2^16
	compare_with2to16:
	    li $t0,0x00010000
	    bge $t3,$t0,end_around_carry
	    j flip_bits
	end_around_carry:
	    srl $t1,$t3,16
	    sll $t3,$t3,16
	    srl $t3,$t3,16
	    add $t3,$t3,$t1
	flip_bits:
	    li $t2,0x0000ffff
	    xor $t2,$t3,$t2 #t2 now has the inverted checksum
	    beqz $t2,return_zero
	return_calculated_checksum:
	    move $v0,$t2
	    jr $ra
	return_zero:
	    li $v0,0
	    jr $ra
	############################################
  

##############################
# PART 2 FUNCTIONS
##############################

extractData:
    #Define your code here
	############################################
	#a0,a1,a2 are used as input
	li $t0,0 #this is an index to the parray
	li $t9,0 #we will set this as the number of bits that is added 
	
	loop_parray:
	    li $t1,60
	    addi $sp,$sp,-4
	    sw $a0,($sp)
	    
	    mul $t2,$t1,$t0
	    add $a0,$a0,$t2 #a0 is initialized in its position
	    
	   
	    checksum_verification:
	    
	    	#store the previous numbers into stack
	        addi $sp,$sp,-16
	        sw $a0,($sp)
	        sw $t0,4($sp)
	        sw $t2,8($sp)
	        sw $ra,12($sp)
	        
	        #call the IPv4 method
	        move $a0,$a0
	        li $t0,0
	        li $t1,0
	        li $t2,0
	        li $t3,0
	        li $t4,0
	        jal verifyIPv4Checksum
	        
	        #restore the stacks
	        lw $a0,($sp)
	        lw $t0,4($sp)
	        lw $t2,8($sp)
	        lw $ra,12($sp)
	        addi $sp,$sp,16
	        
	       	bnez $v0,return_error3
	       	
	    get_payload_length:
	        lh $t3,($a0)
	        andi $t3,$t3,0x0000ffff #t3 now has the total length
	        
	        li $t4,20
	        sub $t3,$t3,$t4 #t3 now has the payload length
	        add $t9,$t9,$t3
	        
	    
	    addi $a0,$a0,20 #skip the header field  
	    li $t4,0 #this is a counter      
	    write_into_msg: #a loop that allows me to write into the msg
	    	lb $t5,($a0)
	    	sb $t5,($a2)
	    	addi $a0,$a0,1
	    	addi $a2,$a2,1
	    	addi $t4,$t4,1
	    	bge $t4,$t3,done_write
	    	j write_into_msg
	   done_write:
	        ###
	        li $t5,10
	        sb $t5,($a2)
	    ###
	    
	    
	    
	    addi $t0,$t0,1
	    lw $a0,($sp)
	    addi $sp,$sp,4
	    bge $t0,$a1,exist_loop_parray
	    
	    j loop_parray
	    
	exist_loop_parray:
	    li $v0,0
	    move $v1,$t9
	    jr $ra
	
	return_error3:
	    lw $a0,($sp)
	    addi $sp,$sp,4
	    li $v0,-1
	    move $v1,$t0
	    jr $ra
	
	
	############################################
    

processDatagram:
    #Define your code here
    #a0,a1,a2 are inputs
    check_M_condition:
        blez $a1,return_error4
        
    li $t0,0 #this will be a index counter
    
    store_words:
        blez $a1,finish_datagram_loop
    	sw $a0,($a2) #store the address into sarray
    	addi $a2,$a2,4	
    	addi $t0,$t0,1
    datagram_loop:
        lb $t1,($a0)
        
        blez $a1,finish_datagram_loop
        addi $a1,$a1,-1
        bne $t1,10,j_loop
        replace_byte:
            
            addi $sp,$sp,-20
            sw $a0,($sp)
            sw $a1,4($sp)
            sw $a2,8($sp)
            sw $t0,12($sp)
            sw $ra,16($sp)
            
            move $a0,$a0
            li $a1,10
            li $a2,0
            jal replace1st
            
            lw $a0,($sp)
            lw $a1,4($sp)
            lw $a2,8($sp)
            lw $t0,12($sp)
            lw $ra,16($sp)
            addi $sp,$sp,20
            
            addi $a0,$a0,1
            j store_words
            
        
        
        j_loop:
        addi $a0,$a0,1
        j datagram_loop
    finish_datagram_loop:
    	#addi $a0,$a0,1
    	li $t1,0
    	sb $t1,($a0)
    	move $v0,$t0
    	jr $ra
    return_error4:
        li $v0,-1
        jr $ra

##############################
# PART 3 FUNCTIONS
##############################

printDatagram:
    #Define your code here
    ############################################
    #a0,a1,a2,a3 are used as input
    check_n_size:
    	blez $a1,return_error5
    	
    check_valid_checksum:
        addi $sp,$sp,-20
        sw $a0,($sp)
        sw $a1,4($sp)
        sw $a2,8($sp)
        sw $a3,12($sp)
       	sw $ra,16($sp)
        
        jal extractData
        
        lw $a0,($sp)
        lw $a1,4($sp)
        lw $a2,8($sp)
        lw $a3,12($sp)
        lw $ra,16($sp)
        addi $sp,$sp,20
        
        #the msg is modified, and v1 has the number of bytes in the msg
        #now check v0 for 0 if all the checksum are valid
        bnez $v0,return_error5
    calling_ProcessDatagram:
    
    	move $a0,$a2
        move $a1,$v1
        move $a2,$a3
        
        
        
        addi $sp,$sp,-8
        sw $ra,($sp)
        sw $a2,4($sp)
       
        
        jal processDatagram
        
        lw $ra,($sp)
        lw $a2,4($sp)
        addi $sp,$sp,8
    	
    	#we only need the msg array to print it, which is a2
    	#v0 is the index end +1 and the length of the array
    	bltz $v0,return_error5
    	
    print_the_string:
    	addi $sp,$sp,-4
    	sw $ra,($sp)
    	
    	move $a0,$a2
    	li $a1,0
    	move $a3,$v0
    	addi $a2,$v0,-1
    	
    	jal printStringArray
    	
    	lw $ra,($sp)
    	addi $sp,$sp,4
    	
    	bltz $v0,return_error5
    	
    return_success5:
        li $v0,0
        jr $ra
    	
    return_error5:
    	li $v0,-1
    	jr $ra

#################################################################
# Student defined data section
#################################################################
.data
.align 2  # Align next items to word boundary
double_newline: .asciiz "\n\n"
#place all data declarations here


