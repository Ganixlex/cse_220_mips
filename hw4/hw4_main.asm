.data
left: .word 'L'
right: .word 'R'
up: .word 'U'
down: .word 'D'
.text

li $a0,0xffff0000
li $a1,6
li $a2,6
li $a3,1
li $t0,0
li $t1,0
li $t2,1
addi $sp,$sp,-12
sw $t0,($sp)
sw $t1,4($sp)
sw $t2,8($sp)
jal start_game
addi $sp,$sp,12

li $a0,0xffff0000
li $a1,6
li $a2,6
li $a3,1
li $t0,1
li $t1,4
addi $sp,$sp,-8
sw $t0,($sp)
sw $t1,4($sp)
jal place
addi $sp,$sp,8

li $a0,0xffff0000
li $a1,6
li $a2,6
li $a3,0
li $t0,0
li $t1,2
addi $sp,$sp,-8
sw $t0,($sp)
sw $t1,4($sp)
jal place
addi $sp,$sp,8

li $a0,0xffff0000
li $a1,6
li $a2,6
li $a3,0
li $t0,2
li $t1,4
addi $sp,$sp,-8
sw $t0,($sp)
sw $t1,4($sp)
jal place
addi $sp,$sp,8


li $a0,0xffff0000
li $a1,6
li $a2,6
li $a3,0
li $t0,3
li $t1,4
addi $sp,$sp,-8
sw $t0,($sp)
sw $t1,4($sp)
jal place
addi $sp,$sp,8

li $a0,0xffff0000
li $a1,6
li $a2,6
li $a3,0
li $t0,1
addi $sp,$sp,-4
sw $t0,($sp)
jal merge_row
addi $sp,$sp,4

li $a0,0xffff0000
li $a1,6
li $a2,6
li $a3,3
li $t0,0
li $t1,4
addi $sp,$sp,-8
sw $t0,($sp)
sw $t1,4($sp)
jal place
addi $sp,$sp,8

li $a0,0xffff0000
li $a1,6
li $a2,6
li $a3,5
li $t0,0
li $t1,4
addi $sp,$sp,-8
sw $t0,($sp)
sw $t1,4($sp)
jal place
addi $sp,$sp,8

li $a0,0xffff0000
li $a1,6
li $a2,6
li $a3,0
li $t0,0
addi $sp,$sp,-4
sw $t0,($sp)
jal shift_col
addi $sp,$sp,4


li $a0,0xffff0000
li $a1,6
li $a2,6
li $a3,4
li $t0,4
li $t1,1024
addi $sp,$sp,-8
sw $t0,($sp)
sw $t1,4($sp)
jal place
addi $sp,$sp,8

li $a0,0xffff0000
li $a1,2
li $a2,2
li $a3,1
li $t0,0
li $t1,2
addi $sp,$sp,-8
sw $t0,($sp)
sw $t1,4($sp)
jal place
addi $sp,$sp,8

li $a0,0xffff0000
li $a1,2
li $a2,2
jal check_state

li $a0,0xffff0000
li $a1,2
li $a2,2
la $a3,up
lw $a3,($a3)
jal user_move
li $a0,0xffff0000
li $a1,2
li $a2,2
la $a3,right
lw $a3,($a3)
jal user_move
li $a0,0xffff0000
li $a1,2
li $a2,2
la $a3,down
lw $a3,($a3)
jal user_move

li $v0,10
syscall

.include "hw4.asm"
